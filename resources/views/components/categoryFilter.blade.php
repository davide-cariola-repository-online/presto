<section class="container card my-5 p-3">
    <div class="row justify-content-center align-items-center mb-5">
        <div class="col-4 text-center">
            <h4>Filtri</h4>
        </div>
    </div>

    <form id="filters" action="{{route('filter.categoryFilters', ['category' => $category])}}" method="GET" >
        @csrf

        {{-- Order By Date--}}
        <div class="row justify-content-center align-items-center">
            <div class="col-4">
                <h5>Ordina per data:</h5>
            </div>
            <div class="col-4">
                    <input type="radio" id="dateDescendantOrder" name="order" value="descendantDateOrder" onclick="document.querySelector('#filters').submit();" {{isset($filters['order']) && $filters['order'] == 'descendantDateOrder' ? 'checked' : ''}}>
                    <label for="dateDescendantOrder">Più recente</label>
                    <br>
                    <input type="radio" id="dateAscendantOrder" name="order" value="ascendantDateOrder" onclick="document.querySelector('#filters').submit();" {{isset($filters['order']) && $filters['order'] == 'ascendantDateOrder' ? 'checked' : ''}}>
                    <label for="dateAscendantOrder">Meno recente</label>
                    <br>
            </div>
            <hr style="width: 50%;" class="my-2">
        </div>

        {{-- Order By Price --}}
        <div class="row justify-content-center align-items-center">
            <div class="col-4">
                <h5>Ordina per prezzo:</h5>
            </div>
            <div class="col-4">
                    <input type="radio" id="priceDescendantOrder" name="order" value="descendantPriceOrder" onclick="document.querySelector('#filters').submit();" {{isset($filters['order']) && $filters['order'] == 'descendantPriceOrder' ? 'checked' : ''}}>
                    <label for="priceDescendantOrder">Prezzo decrescente</label>
                    <br>
                    <input type="radio" id="priceAscendantOrder" name="order" value="ascendantPriceOrder" onclick="document.querySelector('#filters').submit();" {{isset($filters['order']) && $filters['order'] == 'ascendantPriceOrder' ? 'checked' : ''}}>
                    <label for="priceAscendantOrder">Prezzo crescente</label>
                    <br>
            </div>
            <hr style="width: 50%;" class="my-2">
        </div>

        {{-- View By Price Range --}}
        <div class="row justify-content-center align-items-center">
            <div class="col-4">
                <h5>Filtra per prezzo:</h5>
            </div>
            
            <div class="col-4 align-items-center">  
                    {{-- PRICE RANGES --}}
                    {{-- <input id="hidden" name="priceRanges" type="hidden" value="">
                    
                    <input type="checkbox" name="under20" id="under20" onclick="document.querySelector('#hidden').value = 'true'; document.querySelector('#filters').submit();" {{isset($filters['under20']) ? 'checked' : ''}}>
                    <label for="under20">Fino a 20€</label><br>
                    <input type="checkbox" name="from20to50" id="2050" onclick="document.querySelector('#hidden').value = 'true'; document.querySelector('#filters').submit();" {{isset($filters['from20to50']) ? 'checked' : ''}}>
                    <label for="2050">Da 20€ a 50€</label><br>
                    <input type="checkbox" name="from50to100" id="50100" onclick="document.querySelector('#hidden').value = 'true'; document.querySelector('#filters').submit();">
                    <label for="50100">Da 50€ a 100€</label><br>
                    <input type="checkbox" name="from100to200" id="100200" onclick="document.querySelector('#hidden').value = 'true'; document.querySelector('#filters').submit();">
                    <label for="100200">Da 100€ a 200€</label><br>
                    <input type="checkbox" name="from200to500" id="200500" onclick="document.querySelector('#hidden').value = 'true'; document.querySelector('#filters').submit();">
                    <label for="200500">Da 200€ a 500€</label><br>
                    <input type="checkbox" name="over500" id="over500" onclick="document.querySelector('#hidden').value = 'true'; document.querySelector('#filters').submit();">
                    <label for="over500">Da 500€ in sù</label><br> --}}
                

                    <input class="mt-4 mb-2" type="text" id="minPrice" name="minPrice" placeholder="Prezzo Minimo" @if(isset($filters['minPrice'])) value="{{$filters['minPrice']}}"@endif> <br>
                    <input type="text" id="maxPrice" name="maxPrice" placeholder="Prezzo Massimo" @if(isset($filters['maxPrice'])) value="{{$filters['maxPrice']}}"@endif>

                    <div class="col-2">
                        <button type="submit" class="btn btn-primary mt-2" onclick="event.preventDefault(); document.querySelector('#filters').submit();">Vai</button>
                    </div>
            </div>
        </div>
    </form>

</section>