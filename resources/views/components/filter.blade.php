    {{-- GESTIONE TRAMITE MINI-FORM DEDICATI PER FILTRO
    <section class="container card my-5 p-3">
        <div class="row justify-content-center align-items-center">
            <div class="col-4">
                <h4>Filtri:</h4>
                <hr>
            </div>
        </div>


        {{-- View by Category --}}
        {{-- <div class="row justify-content-center align-items-center">
            <div class="col-4">
                <h5>Visualizza per Categoria:</h5>
            </div>
            <div class="col-4">
                <form id="viewByCategory" action="{{route('filter.viewByCategory')}}" method="GET" onchange="document.querySelector('#viewByCategory').submit();">
                    @csrf
                    <select name="categoryId" id="categoryId">
                        <option value="" > - </option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    <br>
                </form>
            </div>
            <hr style="width: 50%;" class="my-2">
        </div> --}}

        {{-- Order By Date--}}
        {{-- <div class="row justify-content-center align-items-center">
            <div class="col-4">
                <h5>Ordina per data:</h5>
            </div>
            <div class="col-4">
                <form id="orderByDate" action="{{route('filter.orderByDate')}}" method="GET">
                    @csrf
                    <input type="radio" id="dateDescendantOrder" name="orderByDate" value="descendantOrder" onclick="document.querySelector('#orderByDate').submit()">
                    <label for="dateDescendantOrder">Più recente</label>
                    <br>
                    <input type="radio" id="dateAscendantOrder" name="orderByDate" value="ascendantOrder" onclick="document.querySelector('#orderByDate').submit()">
                    <label for="dateAscendantOrder">Meno recente</label>
                    <br>
                </form>
            </div>
            <hr style="width: 50%;" class="my-2">
        </div> --}}
    
        {{-- Order By Price --}}
        {{-- <div class="row justify-content-center align-items-center">
            <div class="col-4">
                <h5>Ordina per prezzo:</h5>
            </div>
            <div class="col-4">
                <form id="orderByPrice" action="{{route('filter.orderByPrice')}}" method="GET">
                    @csrf
                    <input type="radio" id="priceDescendantOrder" name="orderByPrice" value="descendantOrder" onclick="document.querySelector('#orderByPrice').submit();">
                    <label for="priceDescendantOrder">Prezzo decrescente</label>
                    <br>
                    <input type="radio" id="priceAscendantOrder" name="orderByPrice" value="ascendantOrder" onclick="document.querySelector('#orderByPrice').submit();">
                    <label for="priceAscendantOrder">Prezzo crescente</label>
                    <br>
                </form>
            </div>
            <hr style="width: 50%;" class="my-2">
        </div> --}}

        {{-- View By Price Range --}}
        {{-- <div class="row justify-content-center align-items-center">
            <div class="col-4">
                <h5>Filtra per prezzo:</h5>
            </div>
            <div class="col-4 d-flex align-items-center">
                <form class="align-items-center" id="viewByPriceRange" action="{{route('filter.viewByPriceRange')}}" method="GET">
                    @csrf

                    <input onclick="document.querySelector('#viewByPriceRange').submit();" type="checkbox" name="range" id="under20" value="under20">
                    <label for="under20">Fino a 20 EUR</label><br>

                    <input onclick="document.querySelector('#viewByPriceRange').submit();" type="checkbox" name="range" id="from20to50" value="from20to50">
                    <label for="from20to50">20 a 50 EUR</label><br>

                    <input onclick="document.querySelector('#viewByPriceRange').submit();" type="checkbox" name="range" id="from50to100" value="from50to100">
                    <label for="from50to100">50 a 100 EUR</label><br>

                    <input onclick="document.querySelector('#viewByPriceRange').submit();" type="checkbox" name="range" id="from100to200" value="from100to200">
                    <label for="from100to200">100 a 200 EUR</label><br>

                    <input onclick="document.querySelector('#viewByPriceRange').submit();" type="checkbox" name="range" id="from200to500" value="from200to500">
                    <label for="from200to500">200 a 500 EUR</label><br>

                    <input onclick="document.querySelector('#viewByPriceRange').submit();" type="checkbox" name="range" id="over500" value="over500">
                    <label for="over500">500 EUR e più</label><br>
                    
                    <input class="mt-4" type="text" id="minPrice" name="minPrice" placeholder="Prezzo Minimo"> <br>
                    <input type="text" id="maxPrice" name="maxPrice" placeholder="Prezzo Massimo">

                    <button type="submit" class="btn btn-primary" onclick="event.preventDefault(); document.querySelector('#viewByPriceRange').submit();">Vai</button>
                </form>
            </div>
            <hr style="width: 50%;" class="my-2">
        </div>
    
    </section>  --}}


    {{-- GESTIONE TRAMITE UNICO FORM --}}
    <section class="container card my-5 p-3">
        <div class="row justify-content-center align-items-center mb-5">
            <div class="col-4 text-center">
                <h4>Filtri</h4>
            </div>
        </div>

        
        {{-- View by Category --}}
        <form id="filters" action="{{route('filter.allFilters')}}" method="GET" >
            @csrf
            <div class="row justify-content-center align-items-center">
                <div class="col-4">
                    <h5>Visualizza per Categoria:</h5>
                </div>
                <div class="col-4">
                        <select name="categoryId" id="categoryId" onchange="document.querySelector('#filters').submit();">
                            <option value="" > - </option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}" {{isset($filters['categoryId']) && $filters['categoryId'] == $category->id ? 'selected' : ''}}>{{ $category->name }}</option>
                            @endforeach
                        </select>
                        <br>
                </div>
                <hr style="width: 50%;" class="my-2">
            </div>

            {{-- Order By Date--}}
            <div class="row justify-content-center align-items-center">
                <div class="col-4">
                    <h5>Ordina per data:</h5>
                </div>
                <div class="col-4">
                        <input type="radio" id="dateDescendantOrder" name="order" value="descendantDateOrder" onclick="document.querySelector('#filters').submit();" {{isset($filters['order']) && $filters['order'] == 'descendantDateOrder' ? 'checked' : ''}}>
                        <label for="dateDescendantOrder">Più recente</label>
                        <br>
                        <input type="radio" id="dateAscendantOrder" name="order" value="ascendantDateOrder" onclick="document.querySelector('#filters').submit();" {{isset($filters['order']) && $filters['order'] == 'ascendantDateOrder' ? 'checked' : ''}}>
                        <label for="dateAscendantOrder">Meno recente</label>
                        <br>
                </div>
                <hr style="width: 50%;" class="my-2">
            </div>
    
            {{-- Order By Price --}}
            <div class="row justify-content-center align-items-center">
                <div class="col-4">
                    <h5>Ordina per prezzo:</h5>
                </div>
                <div class="col-4">
                        <input type="radio" id="priceDescendantOrder" name="order" value="descendantPriceOrder" onclick="document.querySelector('#filters').submit();" {{isset($filters['order']) && $filters['order'] == 'descendantPriceOrder' ? 'checked' : ''}}>
                        <label for="priceDescendantOrder">Prezzo decrescente</label>
                        <br>
                        <input type="radio" id="priceAscendantOrder" name="order" value="ascendantPriceOrder" onclick="document.querySelector('#filters').submit();" {{isset($filters['order']) && $filters['order'] == 'ascendantPriceOrder' ? 'checked' : ''}}>
                        <label for="priceAscendantOrder">Prezzo crescente</label>
                        <br>
                </div>
                <hr style="width: 50%;" class="my-2">
            </div>

            {{-- View By Price Range --}}
            <div class="row justify-content-center align-items-center">
                <div class="col-4">
                    <h5>Filtra per prezzo:</h5>
                </div>
                
                <div class="col-4 align-items-center">  
                        {{-- PRICE RANGES --}}
                        {{-- <input id="hidden" name="priceRanges" type="hidden" value="">
                        
                        <input type="checkbox" name="under20" id="under20" onclick="document.querySelector('#hidden').value = 'true'; document.querySelector('#filters').submit();" {{isset($filters['under20']) ? 'checked' : ''}}>
                        <label for="under20">Fino a 20€</label><br>
                        <input type="checkbox" name="from20to50" id="2050" onclick="document.querySelector('#hidden').value = 'true'; document.querySelector('#filters').submit();" {{isset($filters['from20to50']) ? 'checked' : ''}}>
                        <label for="2050">Da 20€ a 50€</label><br>
                        <input type="checkbox" name="from50to100" id="50100" onclick="document.querySelector('#hidden').value = 'true'; document.querySelector('#filters').submit();">
                        <label for="50100">Da 50€ a 100€</label><br>
                        <input type="checkbox" name="from100to200" id="100200" onclick="document.querySelector('#hidden').value = 'true'; document.querySelector('#filters').submit();">
                        <label for="100200">Da 100€ a 200€</label><br>
                        <input type="checkbox" name="from200to500" id="200500" onclick="document.querySelector('#hidden').value = 'true'; document.querySelector('#filters').submit();">
                        <label for="200500">Da 200€ a 500€</label><br>
                        <input type="checkbox" name="over500" id="over500" onclick="document.querySelector('#hidden').value = 'true'; document.querySelector('#filters').submit();">
                        <label for="over500">Da 500€ in sù</label><br> --}}
                    

                        <input class="mt-4 mb-2" type="text" id="minPrice" name="minPrice" placeholder="Prezzo Minimo" @if(isset($filters['minPrice'])) value="{{$filters['minPrice']}}"@endif> <br>
                        <input type="text" id="maxPrice" name="maxPrice" placeholder="Prezzo Massimo" @if(isset($filters['maxPrice'])) value="{{$filters['maxPrice']}}"@endif>

                        <div class="col-2">
                            <button type="submit" class="btn btn-primary mt-2" onclick="event.preventDefault(); document.querySelector('#filters').submit();">Vai</button>
                        </div>
                </div>
            </div>
        </form>
    
    </section>