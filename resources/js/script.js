const { xor } = require("lodash");

// BACKGROUND COLOR
if (window.location.href.indexOf('/lavora-con-noi') != -1) {
    let body=document.body;
    body.classList.add("body-work");
}

if (window.location.href.indexOf('/annunci/create') != -1) {
    let body=document.body;
    body.classList.add("body-create");
}

if (window.location.href.indexOf('/login') != -1) {
    let body=document.body;
    body.classList.add("body-login");
}

if (window.location.href.indexOf('/register') != -1) {
    let body=document.body;
    body.classList.add("body-register");
}


// GO UP BUTTON
mybutton = document.getElementById("myBtn");
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}


// MINI SEARCHBAR
// myBtn2=document.querySelector("#myBtn2");
// searchContainer=document.querySelector("#searchContainer");
// myBtn2.addEventListener('click', () => {
//   searchContainer.classList.toggle('hide')
//   myBtn2.classList.toggle('unclear')
// })


// VOCAL SEARCH
let searchForm = document.getElementById('searchForm');
let searchInput = document.getElementById('searchInput');
let vocalWrapper = document.getElementById('vocalWrapper');
let SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;

if (searchInput) {

  if(SpeechRecognition) {
    vocalWrapper.innerHTML = `
    <i id="vocalSearch" class="mic me-2 mt-2 tc-accent fas fa-microphone-alt fs-2"></i>
    `;
    console.log('Il browser supporta il riconoscimento vocale');

    let vocalSearch = vocalWrapper.querySelector('i');

    let speech = new SpeechRecognition();
    vocalSearch.addEventListener('click', () => {
      if(vocalSearch.classList.contains("fa-microphone-alt")) {
        speech.start();
      } else {
        speech.stop();
      }
    })

    speech.addEventListener("start", () => {
      vocalSearch.classList.remove("fa-microphone-alt");
      vocalSearch.classList.add("fa-microphone-alt-slash");
      searchInput.focus();
      console.log('AVVIO');
    })

    speech.addEventListener("end", () => {
      vocalSearch.classList.add("fa-microphone-alt");
      vocalSearch.classList.remove("fa-microphone-alt-slash");
      searchInput.focus();
      console.log('SPENTO');
    })

    speech.addEventListener("result", (event) => {
      console.log(event);
      let result = event.results[0][0].transcript;
      searchInput.value = result;
      
      setTimeout(() => {
        searchForm.submit();
      }, 700);
    })

  }

};