<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdController;
use App\Http\Controllers\FilterController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\SocialiteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// GENERALE
Route::get('/', [PublicController::class, 'index'])->name('homepage');
Route::get('/annunci/category/{cat}', [PublicController::class, 'category'])->name('category');
Route::get('/search', [PublicController::class, 'search'])->name('search');
Route::get('/lavora-con-noi', [PublicController::class, 'lavoraConNoi'])->name('lavora-con-noi')->middleware('auth');
Route::post('/lavora-con-noi/submit', [PublicController::class, 'submit'])->name('lavora.submit');
Route::post('/locale/{locale}', [PublicController::class, 'locale'])->name('locale');
Route::get('/profile', [PublicController::class, 'profile'])->name('profile');

// ANNUNCI
Route::get('/annunci/index', [AdController::class, 'index'])->name('ad.index');
Route::get('/annunci/create', [AdController::class, 'create'])->name('ad.create')->middleware('auth');
Route::post('/annunci/store', [AdController::class, 'store'])->name('ad.store')->middleware('auth');
Route::get('/annunci/show/{ad}', [AdController::class, 'show'])->name('ad.show');
Route::get('/annunci/show/{lastFive}', [AdController::class, 'lastFive'])->name('lastFive');
Route::get('/annunci/images', [AdController::class, 'getImages'])->name('ad.getImages');
Route::post('/annunci/images/upload', [AdController::class, 'upload'])->name('ad.upload');
Route::delete('/annunci/images/remove', [AdController::class, 'remove'])->name('ad.remove');

//REVISORE
Route::get('/revisor/index', [RevisorController::class, 'index'])->name('revisor.index');
Route::post('/revisor/ads/{id}/accept', [RevisorController::class, 'accepted'])->name('revisor.accept');
Route::post('/revisor/ads/{id}/reject', [RevisorController::class, 'rejected'])->name('revisor.reject');
Route::get('/revisor/trash', [RevisorController::class, 'trash'])->name('revisor.trash');


// SOCIALITE LOGIN
Route::get('/redirect', [SocialiteController::class, 'redirectToProvider'])->name('google.login');
Route::get('/callback', [SocialiteController::class, 'handleProviderCallback'])->name('google.callback');


// FILTER
Route::get('/annunci/index/orderByDate', [FilterController::class, 'orderByDate'])->name('filter.orderByDate');
Route::get('/annunci/index/orderByPrice', [FilterController::class, 'orderByPrice'])->name('filter.orderByPrice');
Route::get('/annunci/index/viewByCategory', [FilterController::class, 'viewByCategory'])->name('filter.viewByCategory');
Route::get('/annunci/index/viewByPriceRange', [FilterController::class, 'viewByPriceRange'])->name('filter.viewByPriceRange');
Route::get('/annunci/index/filters', [FilterController::class, 'allFilters'])->name('filter.allFilters');
Route::get('/annunci/category/filters/{category}', [FilterController::class, 'categoryFilters'])->name('filter.categoryFilters');


// TEST
Route::get('accept-revisor/{user}', [PublicController::class, 'acceptRevisor'])->name('accept.revisor');