<?php

namespace App\View\Components;

use App\Models\Category;
use Illuminate\View\Component;
use Illuminate\Database\Eloquent\Collection;

class CategoryFilter extends Component
{
    public $category;
    public $filters;

    public function __construct($category, $filters)
    {
        $this->category = $category;
        $this->filters = $filters;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.categoryFilter');
    }
}
