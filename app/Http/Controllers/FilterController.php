<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


class FilterController extends Controller
{

    // Gestione attraverso funzione con tutti i filtri che richiama mini funzioni. Commentati i return dei mini-form iniziali.
    // static function orderByDate(Request $request){
        
    //     switch ($request->orderByDate) {
    //         case 'descendantOrder':
    //             $ads = Ad::where('is_accepted', true)->orderBy('created_at', 'DESC')->get();
    //             return $ads;
    //             // return view('ad.index', compact('ads'));
            
    //         case 'ascendantOrder':
    //             $ads = Ad::where('is_accepted', true)->orderBy('created_at', 'ASC')->get();
    //             return $ads;
    //             // return view('ad.index', compact('ads'));
    //     }


    // }


    // static function orderByPrice(Request $request){

    //     switch ($request->orderByPrice) {
    //         case 'descendantOrder':
    //             $ads = Ad::where('is_accepted', true)->orderBy('price', 'DESC')->get();
    //             return $ads;
    //             // return view('ad.index', compact('ads'));
            
    //         case 'ascendantOrder':
    //             $ads = Ad::where('is_accepted', true)->orderBy('price', 'ASC')->get();
    //             return $ads;
    //             // return view('ad.index', compact('ads'));
    //     }

    // }


    // static function viewByCategory(Request $request){

    //     if($request->categoryId == null){
    //         return redirect(route('ad.index'));
    //     } else {
    //         $ads = Ad::where('is_accepted', true)->where('category_id', $request->categoryId)->get();
    //         return $ads;
    //         // return view('ad.index', compact('ads'));
    //     }
    // }


    // static function viewByPriceRange(Request $request){

    //     switch ($request->range) {
    //         case 'under20':
    //             $ads = Ad::where('is_accepted', true)->where('price', '<', 20)->get();
    //             return $ads;
    //             // return view('ad.index', compact('ads'));

    //         case 'from20to50':
    //             $ads = Ad::where('is_accepted', true)->whereBetween('price', [20, 50])->get();
    //             return $ads;
    //             // return view('ad.index', compact('ads'));

    //         case 'from50to100':
    //             $ads = Ad::where('is_accepted', true)->whereBetween('price', [50, 100])->get();
    //             return $ads;
    //             // return view('ad.index', compact('ads'));

    //         case 'from100to200':
    //             $ads = Ad::where('is_accepted', true)->whereBetween('price', [100, 200])->get();
    //             return $ads;
    //             // return view('ad.index', compact('ads'));

    //         case 'from200to500':
    //             $ads = Ad::where('is_accepted', true)->whereBetween('price', [200, 500])->get();
    //             return $ads;
    //             // return view('ad.index', compact('ads'));

    //         case 'over500':
    //             $ads = Ad::where('is_accepted', true)->where('price', '>', 500)->get();
    //             return $ads;
    //             // return view('ad.index', compact('ads'));

    //         default:
    //             $ads = Ad::where('is_accepted', true)->whereBetween('price', [$request->minPrice, $request->maxPrice])->get();
    //             return $ads;
    //             // return view('ad.index', compact('ads'));
    //     }

    // }


    public function allFilters(Request $request){
        // dd($request->all());
        $query = Ad::query();

        $query->when($request->categoryId, function ($q, $categoryId) {
            return $q->where('category_id', $categoryId);
        })->when($request->order == 'descendantDateOrder', function($q){
            return $q->orderBy('created_at', 'DESC');
        })->when($request->order == 'ascendantDateOrder', function($q){
            return $q->orderBy('created_at', 'ASC');
        })->when($request->order == 'descendantPriceOrder', function($q){
            return $q->orderBy('price', 'DESC');
        })->when($request->order == 'ascendantPriceOrder', function($q){
            return $q->orderBy('price', 'ASC');
        })->when($request->minPrice && $request->maxPrice, function ($q, $value) use ($request){
            return $q->whereBetween('price', [$request->minPrice, $request->maxPrice]);
        });
        
        
        //PRICE RANGES
        // ->when($request->priceRanges == true, function ($q) use ($request){
        //     return $q->where('price', '<', 20) && $q->whereBetween('price', [20, 50]);
        // });

        // under20, from20to50, from50to100, from100to200, from200to500, over500

        $ads = $query->get();


        $filters = $request->all();

        return view('ad.index', compact('filters', 'ads'));

    }


    // public function categoryFilters(Request $request, $category){

    //     // Gestione dovuta ad aggiunte successive fuori dalla progettazione iniziale
    //     $cat = $category;
    //     $categoryIntermediate = Category::where('id', $cat)->pluck('name');
    //     $category = $categoryIntermediate[0];
    //     // Gestione dovuta ad aggiunte successive fuori dalla progettazione iniziale
        

    //     $query = Ad::query();

    //     $query->where('category_id', $cat)
    //         ->when($request->order == 'descendantDateOrder', function($q){
    //         return $q->orderBy('created_at', 'DESC');
    //     })->when($request->order == 'ascendantDateOrder', function($q){
    //         return $q->orderBy('created_at', 'ASC');
    //     })->when($request->order == 'descendantPriceOrder', function($q){
    //         return $q->orderBy('price', 'DESC');
    //     })->when($request->order == 'ascendantPriceOrder', function($q){
    //         return $q->orderBy('price', 'ASC');
    //     })->when($request->minPrice && $request->maxPrice, function ($q, $value) use ($request){
    //         return $q->whereBetween('price', [$request->minPrice, $request->maxPrice]);
    //     });

    //     $ads = $query->get();


    //     $filters = $request->all();

    //     return view('category', compact('filters', 'ads', 'cat', 'category'));

    // }


}
